#include "impl.hpp"
#include <sstream>

bool threading::Thread::start(const ThreadPtr& t) {
    t->id = ThreadsImpl::instance()->start(t);
    return t->id != 0;
}

threading::Thread::Thread() = default;

threading::Thread::Thread(threading::RunnablePtr r) : r(std::move(r)) {}

threading::Thread::Thread(threading::Thread && t) noexcept : r(std::move(t.r)), id(t.id) {
    t.id = 0;
}

threading::Thread::~Thread() = default;

threading::Thread &threading::Thread::operator=(threading::Thread && t) noexcept {
    if (this != &t) {
        r = std::move(t.r);
        id = t.id;
        t.id = 0;
    }
    return *this;
}

void threading::Thread::run() {
    if (r)
        r->run();
}

bool threading::Thread::isRunning() {
    return ThreadsImpl::instance()->isRunning(id);
}

bool threading::Thread::interrupt() {
    return ThreadsImpl::instance()->interrupt(id);
}

void threading::Thread::release() {
    ThreadsImpl::instance()->release(id);
}

void threading::Thread::wait() {
    ThreadsImpl::instance()->join(id);
}

void threading::Thread::sleep(uint64_t milliseconds) {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}

unsigned long threading::Thread::getCurrentId() {
    std::thread::native_handle_type id {0};
    try {
        std::stringstream ss;
        ss << std::this_thread::get_id();
        std::stringstream idin(ss.str());
        idin >> id;
    } catch (...) {}
    return id;
}
