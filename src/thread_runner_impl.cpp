#include "impl.hpp"

threading::ThreadRunner::ThreadRunner() = default;

threading::ThreadRunner::~ThreadRunner() = default;

bool threading::ThreadRunner::start(const threading::ThreadPtr & ptr) {
    return Thread::start(ptr);
}
