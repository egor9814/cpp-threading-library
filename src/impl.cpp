#include "impl.hpp"

ThreadsImpl * ThreadsImpl::instance() {
    static ThreadsImpl t;
    return &t;
}

static void invoker(const threading::ThreadPtr& t) {
    t->run();
}
std::thread::native_handle_type ThreadsImpl::start(const threading::ThreadPtr &ptr) {
    auto t = new std::thread(invoker, ptr);
    if (t->native_handle() != 0)
        instance()->threads[t->native_handle()] = info { t, ptr, true, false };
    return t->native_handle();
}

bool ThreadsImpl::isRunning(std::thread::native_handle_type id) {
    return threads.count(id) != 0;
}

void ThreadsImpl::join(std::thread::native_handle_type id) {
    if (threads.count(id)) {
        auto &t = threads[id].native;
        if (t->joinable())
            t->join();
    }
}

void ThreadsImpl::release(std::thread::native_handle_type id) {
    if (threads.count(id)) {
        threads[id].joining = false;
    }
}

bool ThreadsImpl::checkInterruption(std::thread::native_handle_type id) {
    return threads.count(id) && threads[id].interrupted;
}

bool ThreadsImpl::interrupt(std::thread::native_handle_type id) {
    if (threads.count(id)) {
        threads[id].interrupted = true;
        return true;
    }
    return false;
}

void ThreadsImpl::registerMutex(threading::Mutex *m) noexcept {
    auto result = std::make_shared<std::mutex>();
    if (mutexes.count(m)) {
        mutexes[m]->unlock();
    }
    mutexes[m] = result;
}

void ThreadsImpl::unregisterMutex(threading::Mutex *m) {
    if (mutexes.count(m)) {
        mutexes[m]->unlock();
        mutexes.erase(m);
    }
}

bool ThreadsImpl::lockMutex(threading::Mutex *m) {
    if (mutexes.count(m)) {
        if (mutexes[m]) {
            return mutexes[m]->try_lock();
        }
    }
    return false;
}

bool ThreadsImpl::unlockMutex(threading::Mutex *m) {
    if (mutexes.count(m)) {
        if (mutexes[m]) {
            mutexes[m]->unlock();
            return true;
        }
    }
    return false;
}

ThreadsImpl::~ThreadsImpl() {
    for (auto &it : threads) {
        if (it.second.native) {
            if (it.second.joining && it.second.native->joinable()) {
                it.second.native->join();
            }
            delete it.second.native;
        }
    }
    for (auto &it : mutexes) {
        if (it.second) {
            it.second->unlock();
        }
    }
}


bool threading::__check_interruption_section() {
    return ThreadsImpl::instance()->checkInterruption(threading::Thread::getCurrentId());
}
