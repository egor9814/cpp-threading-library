#include "impl.hpp"

threading::Mutex::Mutex() noexcept {
    ThreadsImpl::instance()->registerMutex(this);
}

threading::Mutex::~Mutex() {
    ThreadsImpl::instance()->unregisterMutex(this);
}

bool threading::Mutex::lock() {
    return ThreadsImpl::instance()->lockMutex(this);
}

bool threading::Mutex::unlock() {
    return ThreadsImpl::instance()->unlockMutex(this);
}
