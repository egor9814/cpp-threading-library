#ifndef __egor9814__cpp_threading__impl_hpp__
#define __egor9814__cpp_threading__impl_hpp__

#include <threading.hpp>
#include <thread>
#include <mutex>
#include <map>

struct ThreadsImpl {
    struct info {
        std::thread* native;
        threading::ThreadPtr ptr;
        bool joining;
        bool interrupted;
    };
    std::map<std::thread::native_handle_type, info> threads;

    std::map<threading::Mutex*, std::shared_ptr<std::mutex>> mutexes;

    static ThreadsImpl* instance();

    std::thread::native_handle_type start(const threading::ThreadPtr& ptr);

    bool isRunning(std::thread::native_handle_type id);

    void join(std::thread::native_handle_type id);

    void release(std::thread::native_handle_type id);

    bool checkInterruption(std::thread::native_handle_type id);

    bool interrupt(std::thread::native_handle_type id);

    void registerMutex(threading::Mutex* m) noexcept;

    void unregisterMutex(threading::Mutex* m);

    bool lockMutex(threading::Mutex* m);

    bool unlockMutex(threading::Mutex* m);

    ~ThreadsImpl();
};

#endif //__egor9814__cpp_threading__impl_hpp__
