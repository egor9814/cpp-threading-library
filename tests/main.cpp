#include <threading.hpp>
#include <iostream>

static threading::Mutex logger;

class MyThread : public threading::Thread {
public:
    void run() override {
        for (int i = 0; i < 10; i++) {
            synchronized(logger)
                std::cout << "id: " << threading::Thread::getCurrentId() << "; #" << i << std::endl;
            synchronized_end()
            Thread::sleep(1000);
            InterruptionSection();
        }
    }
};

static void worker() {
    synchronized(logger)
        std::cout << "id: " << threading::Thread::getCurrentId() << "; Work on" << std::endl;
    synchronized_end()
    threading::Thread::sleep(15000);
    synchronized(logger)
        std::cout << "id: " << threading::Thread::getCurrentId() << "; Work off" << std::endl;
    synchronized_end()
}

int main() {
    auto t = std::make_shared<MyThread>();
    threading::ThreadRunner::start(t);
    threading::ThreadRunner::start(std::make_shared<threading::Thread>(worker));
    threading::Thread::sleep(5000);
    t->interrupt();
    return 0;
}
