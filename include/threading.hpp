#ifndef __egor9814__cpp_threading__thread_hpp__
#define __egor9814__cpp_threading__thread_hpp__

#include <memory>

namespace threading {

    struct Runnable {
        virtual void run() = 0;
    };
    using RunnablePtr = std::shared_ptr<Runnable>;


    class ThreadRunner;
    class Thread;
    using ThreadPtr = std::shared_ptr<Thread>;

    class Thread {
        friend class ThreadRunner;

        RunnablePtr r{nullptr};

        template <typename Callable>
        struct CallableRunnable : Runnable {
            Callable c;
            explicit CallableRunnable(Callable&& c) : c(std::forward<Callable>(c)) {}
            void run() override {
                c();
            }
        };
        template <typename Callable>
        static RunnablePtr makeRunnable(Callable&& c) {
            using CR = CallableRunnable<Callable>;
            return std::make_shared<CR>(std::forward<Callable>(c));
        }

        static bool start(const ThreadPtr&);

        unsigned long id{0};
    public:
        Thread();
        explicit Thread(RunnablePtr);

        template <typename Callable>
        explicit Thread(Callable&& c) : r(makeRunnable(std::forward<Callable>(c))) {}

        Thread(const Thread&) = delete;
        Thread(Thread&&) noexcept;

        ~Thread();

        Thread&operator=(const Thread&) = delete;
        Thread&operator=(Thread&&) noexcept;

        virtual void run();

        bool isRunning();

        bool interrupt();

        void release();

        void wait();

        static void sleep(uint64_t milliseconds);

        static unsigned long getCurrentId();
    };


    class ThreadRunner final {
        friend class Thread;

        ThreadRunner();
        ~ThreadRunner();

    public:
        static bool start(const ThreadPtr &ptr);
    };


    bool __check_interruption_section();


    struct Lockable {
        virtual bool lock() = 0;
        virtual bool unlock() = 0;
    };

    class Mutex : Lockable {
    public:
        Mutex() noexcept ;
        ~Mutex();

        bool lock() override;
        bool unlock() override;

        Mutex(const Mutex&) = delete;
        Mutex(Mutex&&) = delete;

        Mutex&operator=(const Mutex&) = delete;
        Mutex&operator=(Mutex&&) noexcept = delete;
    };

    template <typename T /* : Lockable */>
    struct LockGuard {
        using LockableType = T;
        static_assert(std::is_base_of<Lockable, LockableType>::value, "LockGuard::Type must be inherit Lockable");

    private:
        LockableType* lock {nullptr};

    public:
        explicit LockGuard(LockableType& l) {
            l.lock();
            lock = &l;
        }

        ~LockGuard() {
            if (lock)
                lock->unlock();
        }
    };

}

#define InterruptionSection() if (::threading::__check_interruption_section()) return

#define __private_synchronized__(LOCK)\
    do {\
        ::threading::LockGuard<decltype(LOCK)> guard(LOCK);
#define __private_synchronized_end__ } while (0);

#define synchronized(LOCK) __private_synchronized__(LOCK)
#define synchronized_end() __private_synchronized_end__

#endif //__egor9814__cpp_threading__thread_hpp__
